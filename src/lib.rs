#![feature(proc_macro)]

extern crate proc_macro;
extern crate ssexp;

use proc_macro::{
    TokenStream,
    //TokenKind
};

use std::str::{
    FromStr
};
use std::collections::HashMap;

use ssexp::{
    Token,
    List,
    Symbol,
    parse_sexp,
    parse_list_inline,
    MacroMap
};



fn m_let(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    assert!(0 < len && len < 3, "Wrong number of arguments");
    
    let mut string = String::from_str("let ").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);
    
    if len == 2 {
        let value = expand(&toks[1], map);
        string.push_str("=");
        string.push_str(&value[..]);
    }
    
    string
}

fn m_if(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    assert!(1 < len && len < 4, "Wrong number of arguments");
    
    let mut string = String::from_str("if ").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);

    let name = expand(&toks[1], map);
    
    string.push_str("{");
    string.push_str(&name[..]);
    string.push_str("}");
    
    if len == 3 {
        string.push_str("else{");
        let value = expand(&toks[2], map);
        string.push_str(&value[..]);
        string.push_str("}");
    }
    
    string
}

fn m_if_let(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    assert!(1 < len && len < 4, "Wrong number of arguments");
    
    let mut string = String::from_str("if let").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);

    let name = expand(&toks[1], map);
    
    string.push_str("{");
    string.push_str(&name[..]);
    string.push_str("}");
    
    if len == 3 {
        string.push_str("else{");
        let value = expand(&toks[2], map);
        string.push_str(&value[..]);
        string.push_str("}");
    }
    
    string
}

fn m_use(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    assert!(0 < len && len < 2, "Wrong number of arguments");
    
    let mut string = String::from_str("use ").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);
    
    string
}

fn m_mut(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    assert!(0 < len && len < 3, "Wrong number of arguments");
    
    let mut string = String::from_str("let mut ").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);
    
    if len == 2 {
        let value = expand(&toks[1], map);
        string.push_str("=");
        string.push_str(&value[..]);
    }
    
    string
}

fn m_set(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    assert!(1 < len && len < 3, "Wrong number of arguments");
    
    let mut string = String::from_str("").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);
    
    if len == 2 {
        let value = expand(&toks[1], map);
        string.push_str("=");
        string.push_str(&value[..]);
    }
    
    string
}

fn m_scope(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    let mut string = String::from_str("{").unwrap();

    let mut first = true;

    for tok in toks.iter() {
        if first {
            first = false;
        } else {
            string.push_str(";");
        }
        let value = expand(tok, map);
        string.push_str(&value[..]);
    }
    string.push_str("}");
    string
}

fn m_fn(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    let mut string = String::from_str("fn ").unwrap();
    
    let name = expand(&toks[0], map);
    
    string.push_str(&name[..]);

    if let &List(ref val_list) = &toks[1] {
        //string.push_str(&name[..]);
    
    } else {
        panic!("Fn value list incorrect")
    }
    
    let mut first = true;

    for tok in toks[2..].iter() {
        if first {
            first = false;
        } else {
            string.push_str(";");
        }
        let value = expand(tok, map);
        string.push_str(&value[..]);
    }
    string.push_str("}");
    string
}

fn m_loop(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    let mut string = String::from_str("loop{").unwrap();

    let mut first = true;

    for tok in toks.iter() {
        if first {
            first = false;
        } else {
            string.push_str(";");
        }
        let value = expand(tok, map);
        string.push_str(&value[..]);
    }
    string.push_str("}");
    string
}

fn m_ign(toks: &[Token], map: &mut FnMap) -> String {
    String::from_str("()").unwrap()
}

fn m_string(toks: &[Token], map: &mut FnMap) -> String {
    format!(r#""{}""#, toks[0])
}

fn m_include(toks: &[Token], map: &mut FnMap) -> String {
    if let Symbol(ref name) = toks[0] {
        use std::fs::File;
        let file = File::open(name).unwrap();
        let pmap = MacroMap::new()
            .with_lists('(')
            .with_comments(';')
            .with_strings('"')
            .with_seperating_whitespaces();
        let sexps = parse_sexp(file,parse_list_inline,pmap);
        let token = List(sexps);
        expand(&token,map)
    } else {
        panic!("Filename needed for include, got list")
    }
}

fn m_of(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    let mut string = String::from_str("").unwrap();

    let mut first = true;

    for tok in toks.iter() {
        if first {
            first = false;
        } else {
            string.push_str("::");
        }
        let value = expand(tok, map);
        string.push_str(&value[..]);
    }
    string.push_str("");
    string
}

fn m_fn_call(id: String, toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    let mut string = String::from_str("").unwrap();

    string.push_str(&id[..]);

    let mut first = true;

    string.push_str("(");

    for tok in toks.iter() {
        if first {
            first = false;
        } else {
            string.push_str(",");
        }
        let value = expand(tok, map);
        string.push_str(&value[..]);
    }
    string.push_str(")");
    string
}

fn m_rust_macro(toks: &[Token], map: &mut FnMap) -> String {
    let len = toks.len();
    
    let mut string = String::from_str("").unwrap();

    let value = expand(&toks[0], map);
    string.push_str(&value[..]);

    let mut first = true;

    string.push_str("!(");

    for tok in toks[1..].iter() {
        if first {
            first = false;
        } else {
            string.push_str(",");
        }
        let value = expand(tok, map);
        string.push_str(&value[..]);
    }
    string.push_str(")");
    string
}

type Expander = fn(&[Token], &mut FnMap) -> String;

struct FnMap<'a>(HashMap<&'a str, Expander>);

fn expand<'a>(token: &Token, map: &mut FnMap<'a>) -> String {
    match token {
        &Symbol(ref string) => String::from_str(string).unwrap(),
        &List(ref vec) => {
            let id = expand(&vec[0], map);
            let some = {
                let fun = map.0.get(&id[..]);
                fun.is_some()
            };
            if some {
                map.0[&id[..]](&vec[1..], map)
            } else {
                m_fn_call(id, &vec[1..], map)
            }
        }
    }
}



#[proc_macro]
pub fn lisp(input: TokenStream) -> TokenStream {
    let string = input.to_string();
    let read = string.as_bytes();
    let map = MacroMap::new()
        .with_lists('(')
        .with_comments(';')
        .with_strings('"')
        .with_seperating_whitespaces();
    let sexps = parse_sexp(read,parse_list_inline,map);
    let token = List(sexps);
    
    let mut map = HashMap::new();
    map.insert("scope", m_scope as fn(&[Token], &mut FnMap) -> String);
    map.insert("let", m_let as fn(&[Token], &mut FnMap) -> String);
    map.insert("set", m_set as fn(&[Token], &mut FnMap) -> String);
    map.insert("mut", m_mut as fn(&[Token], &mut FnMap) -> String);
    map.insert("use", m_use as fn(&[Token], &mut FnMap) -> String);
    map.insert("of", m_of as fn(&[Token], &mut FnMap) -> String);
    map.insert("if", m_if as fn(&[Token], &mut FnMap) -> String);
    map.insert("if_let", m_if_let as fn(&[Token], &mut FnMap) -> String);
    map.insert("rust_macro", m_rust_macro as fn(&[Token], &mut FnMap) -> String);
    map.insert("string", m_string as fn(&[Token], &mut FnMap) -> String);
    map.insert("loop", m_loop as fn(&[Token], &mut FnMap) -> String);
    map.insert("ign", m_ign as fn(&[Token], &mut FnMap) -> String);
    map.insert("include", m_include as fn(&[Token], &mut FnMap) -> String);
    let rust = &expand(&token, &mut FnMap(map))[..];
    println!{"rust {}", rust};
    TokenStream::from_str(rust).unwrap()
}



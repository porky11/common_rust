#![feature(use_extern_macros)]

#[macro_use]
extern crate common_rust;

#[test]
fn test1() {
    //let mut x = 1;
    common_rust::lisp!
      (scope
        (mut x 1)
        (use (of std ops Add))
        (let y ((of Add add) 1 2))
        (set x y)
        (rust_macro assert_eq x 3))
}

#[test]
fn test2() {
    common_rust::lisp!
      (scope
        (mut x false)
        (set x (if x false true))
        (rust_macro assert_eq x true));
}

#[test]
fn test_string() {
    common_rust::lisp!
      (scope
        (let x "test")
        (rust_macro assert_eq x "test"));
}

#[test]
fn test_include() {
    common_rust::lisp!
      (scope
        (mut x 0)
        (include file0)
        (rust_macro assert_eq x 1));
}

